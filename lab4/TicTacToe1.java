import java.io.IOException;
import java.util.Scanner;

        public class tictactoe {

			public static void main(String[] args) throws IOException {
				Scanner reader = new Scanner(System.in);
				char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};


				printBoard(board);

                int player = 0;
                boolean gameEnded = false;
                int validMove = 0;
				while(!gameEnded) {
					System.out.print("Player " + (player+1) +"enter row number:");
					int row = reader.nextInt();
					System.out.print("Player" + (player+1) + "enter column number:");
					int col = reader.nextInt();
					if(isValid(row,col) && board[row - 1][col - 1] == ' ') {
						validMove++;

						board[row - 1][col - 1] = (player == 0 ? 'X' : '0');
						printBoard(board);
						player = ++player % 2;
					}else {
						System.out.println("Wrong Coorinates!!");
					}
					gameEnded= validMove ==9;


				}

				reader.close();
			}

			private static boolean isValid(int row, int col) {
				boolean valid = true;
				if(row < 1 || row > 3)
					return false;
				if (col <1 || col>3)
					return false;
				return true;

			}

			public static void printBoard(char[][] board) {
				System.out.println("    1   2   3");
				System.out.println("   -----------");
				for (int row = 0; row < 3; ++row) {
					System.out.print(row + 1 + " ");
					for (int col = 0; col < 3; ++col) {
						System.out.print("|");
						System.out.print(" " + board[row][col] + " ");
						if (col == 2)
							System.out.print("|");

					}
					System.out.println();

					System.out.println("   -----------");

				}


			}

		}
