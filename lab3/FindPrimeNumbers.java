//exercise5
import java.util.Scanner;

public class FindPrimeNumbers {

    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        System.out.println("give me a number");
        int number = reader.nextInt();
        int sayac = 0;
        for(int sayi=2;sayi<=number;sayi++)
        {
            int kontrol = 0;
            for (int i = 2; i < sayi; i++)
            {
                if (sayi % i == 0)
                {
                    kontrol = 1;
                    break;
                }
            }

            if(kontrol==0)
            {
                System.out.print(sayi+"\n");
                sayac++;
            }
        }
    }
}