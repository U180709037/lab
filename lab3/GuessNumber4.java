//exercise4
import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber4 {

    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in); //Creates an object to read user input
        Random rand = new Random(); //Creates an object from Random class
        int number = rand.nextInt(100); //generates a number between 0 and 99

        int guess;

        System.out.println("Hi! I'm thinking of a number between 0 and 99.");
        System.out.print("Can you guess it: ");

        do {
            guess = reader.nextInt();; //Read the user input
            int count=0;
            count=count +1;
            if (guess == number) {
                System.out.println("Congratulations,you found number after "+count+" attempts");
            }


            else{
                System.out.println("Type -1 to quit or guess another");
            }
            if (guess < number)

                System.out.println("Sorry Your guess is smaller than the secret number.");

            else if (guess > number)

                System.out.println("Sorry Your guess is greater than the secret number.");
            if(guess == -1){
                System.out.println("Sorry the number was"+number);}




        } while(guess !=number);


        reader.close(); //Close the resource before exiting
    }


}
